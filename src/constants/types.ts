export type FeedResponse = {
  element_count: number,
  links: Links,
  near_earth_objects: Record<string, NearEarthObjectResponse>[],
}

export type Links = {
  next?: string,
  prev?: string,
  self: string,
}

export type NearEarthObjectResponse = {
  absolute_magnitude_h: number,
  close_approach_data: CloseApproachData[],
  estimated_diameter: { meters: {estimated_diameter_min: number, estimated_diameter_max: number } }
  id: string,
  is_potentially_hazardous_asteroid: boolean,
  is_sentry_object: boolean,
  links: Links,
  name: string,
  nasa_jpl_url: string,
  neo_reference_id: string,
}

export type CloseApproachData = {
  close_approach_date: string,
  close_approach_date_full: string,
  relative_velocity: { kilometers_per_hour: string },
  miss_distance: { kilometers: string }
}

export type NearEarthObject = {
  id: string,
  name: string,
  estimated_diameter: { min: number, max: number }
  is_potentially_hazardous: boolean,
  is_sentry_object: boolean,
  relative_velocity: number,
  miss_distance: number,
  absolute_magnitude_h: number,
  visual_magnitude?: number,
  close_approach_date: string,
  close_approach_date_full: string,
}