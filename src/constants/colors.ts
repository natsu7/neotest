export default {
  black: '#000000',
  white: '#FFFFFF',
  red: '#D8000C',
  accentBlue: '#109CF1',
  rockBlue: '#90A0B7',
  denimBlue: '#334D6E',
  blue: '#2F80ED',
  gray: 'rgba(0, 0, 0, 0.2)',
  transparent: 'rgba(0,0,0,0)',
  header: 'rgba(248, 248, 248, 0.92)',
  search: 'rgba(0, 0, 0, 0.36)',
  backgroundGray: 'rgba(255, 255, 255, 0.92)',
  searchBackground: 'rgba(112, 118, 131, 0.12)',
};