import { combineReducers } from 'redux';

import feedReducer from '../../ducks/feed';

const rootReducer = combineReducers({
  feed: feedReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

export default rootReducer;
