import { NearEarthObject } from '../../constants/types';
import * as types from './actionTypes';
import { Actions } from './actions';

interface FeedState {
  NEOfeed: NearEarthObject[];
  isGettingNEOFeed: boolean;
}

const initialState = {
  NEOfeed: [],
  isGettingNEOFeed: false,
};

export default (
  state: FeedState = initialState,
  action: Actions,
): FeedState => {
  switch (action.type) {
    case types.GET_NEO_FEED_REQUEST:
      return {
        ...state,
        isGettingNEOFeed: true,
      };

    case types.GET_NEO_FEED_SUCCESS:
      return {
        ...state,
        NEOfeed: action.NEOfeed,
        isGettingNEOFeed: false,
      };

    case types.GET_NEO_FEED_FAILURE:
      return {
        ...state,
        isGettingNEOFeed: false,
        NEOfeed: [],
      };

    default:
      return state;
  }
};
