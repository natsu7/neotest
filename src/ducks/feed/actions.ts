import { NearEarthObject } from '../../constants/types';
import * as types from './actionTypes';


export const getNEOFeedRequest = (data: { start_date: string, end_date: string }) => ({
  type: types.GET_NEO_FEED_REQUEST,
  data,
});

export const getNEOFeedSuccess = (NEOfeed: NearEarthObject[]) => ({
  type: types.GET_NEO_FEED_SUCCESS,
  NEOfeed,
});

export const getNEOFeedFailure = () => ( {
  type: types.GET_NEO_FEED_FAILURE,
});


export type Actions = ReturnType<
  | typeof getNEOFeedRequest
  | typeof getNEOFeedSuccess
  | typeof getNEOFeedFailure
  >;
