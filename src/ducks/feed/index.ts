import reducer from './reducer';

import * as feedActions from './actions';
import * as feedActionTypes from './actionTypes';

export { feedActions, feedActionTypes };

export default reducer;
