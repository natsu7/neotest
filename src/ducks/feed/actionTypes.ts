export const GET_NEO_FEED_REQUEST = '[Feed]/GET_NEO_FEED_REQUEST' as const;
export const GET_NEO_FEED_SUCCESS = '[Feed]/GET_NEO_FEED_SUCCESS' as const;
export const GET_NEO_FEED_FAILURE = '[Feed]/GET_NEO_FEED_FAILURE' as const;