import { get } from 'lodash';

import { NearEarthObject } from '../../constants/types';
import { AppState } from '../../config/redux/rootReducer';

export const getNEOFeed = (state: AppState): NearEarthObject[] => get(state, 'feed.NEOfeed', []);

// _______________________________________

export const getIsGettingNEOFeed = (state: AppState):boolean => get(state, 'feed.isGettingNEOFeed', false);