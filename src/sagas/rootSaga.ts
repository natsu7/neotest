
import { takeEvery } from 'redux-saga/effects';

import * as feedActionTypes from '../ducks/feed/actionTypes';
import { getNEOFeed } from './feedSaga';

export default function* rootSaga() {
  try {
    yield takeEvery(feedActionTypes.GET_NEO_FEED_REQUEST, getNEOFeed);
  } catch (error) {
    console.log('rootSaga error', error);
  }
}
