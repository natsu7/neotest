import { call, put } from 'redux-saga/effects';
import { AnyAction } from 'redux';
import { flatten } from 'lodash';

import {
  getNEOFeedSuccess,
  getNEOFeedFailure,
} from '../ducks/feed/actions';

import { ServiceAPI as api } from '../services';
import { mapNEOresponse } from '../utils/helpers';
import { NearEarthObjectResponse } from '../constants/types';


export function* getNEOFeed({ data = {} }: AnyAction) {
  try {
    const response = yield call(api.getFeed, data);
    if (response.ok) {
      const NEOArray: NearEarthObjectResponse[] = flatten(Object.values(response.data.near_earth_objects));
      const NEOMapped = mapNEOresponse(NEOArray);
      yield put(getNEOFeedSuccess(NEOMapped));
    } else {
      yield put(getNEOFeedFailure());
    }
  } catch (error) {
    yield put(getNEOFeedFailure());
    console.warn('error', error);
  }
}
