import moment from 'moment';

export const formatDate = (date: string | Date, formatStyle: string) => moment(date).format(formatStyle);

export const formatDateForAPI = (date: string | Date) => formatDate(date, 'YYYY-MM-DD');

export const formatDateForDisplay = (date: string | Date) => formatDate(date, 'DD.MM.YYYY');