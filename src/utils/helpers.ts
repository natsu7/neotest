import { get, round } from 'lodash';

import { NearEarthObject, NearEarthObjectResponse } from '../constants/types';

export const mapNEOresponse = (neos: NearEarthObjectResponse[]): NearEarthObject[] => neos.map(item => ({
  id: item.id,
  name: item.name,
  estimated_diameter: {
    min: round(get(item, 'estimated_diameter.meters.estimated_diameter_min'), 3),
    max: round(get(item, 'estimated_diameter.meters.estimated_diameter_max'), 3),
  },
  is_potentially_hazardous: item.is_potentially_hazardous_asteroid,
  is_sentry_object: item.is_sentry_object,
  relative_velocity: round(get(item, 'close_approach_data[0].relative_velocity.kilometers_per_hour'), 3),
  miss_distance: round(get(item, 'close_approach_data[0].miss_distance.kilometers'), 3),
  absolute_magnitude_h: item.absolute_magnitude_h,
  // visual_magnitude: number,
  close_approach_date: get(item, 'close_approach_data[0].close_approach_date'),
  close_approach_date_full: get(item, 'close_approach_data[0].close_approach_date_full'),
}));


export const convertBoolToText = (value: boolean) => value ? 'Yes' : 'No';