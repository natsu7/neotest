import { isEmpty } from 'lodash';
import React, { Component } from 'react';
import { View } from 'react-native';

import { NearEarthObject } from '../../constants/types';
import { NEOList, LoadingWrapper, DatePicker, SearchBar } from './components';
import styles from './feed.styles';

interface Props {
  data: NearEarthObject[];
  isGettingFeed: boolean;
  startDate: Date;
  endDate: Date;
  searchValue: string;
  isRefreshing: boolean;
  onRefresh: () => void;
  onSelectStartDate: (date: Date) => void;
  onSelectEndDate: (date: Date) => void;
  onChangeText: (text: string) => void;
  onClearText: () => void;
}

class FeedScreen extends Component<Props> {

  render() {
    const {
      data,
      isGettingFeed,
      isRefreshing,
      startDate,
      endDate,
      searchValue,
      onRefresh,
      onSelectStartDate,
      onSelectEndDate,
      onChangeText,
      onClearText,
    } = this.props;

    return (
      <View style={styles.container}>
        <SearchBar
          value={searchValue}
          onChangeText={onChangeText}
          onClearPress={onClearText}
        />
        <View style={styles.datePickersContainer}>
          <DatePicker
            date={startDate}
            placeholderText="select start date"
            onDateSelected={onSelectStartDate}
          />
          <DatePicker
            date={endDate}
            placeholderText="select end date"
            onDateSelected={onSelectEndDate}
          />
        </View>

        <LoadingWrapper isLoading={isGettingFeed && isEmpty(data)} size="large">
          <NEOList
            data={data}
            isRefreshing={isRefreshing}
            onRefresh={onRefresh}
          />
        </LoadingWrapper>
      </View>
    );
  }
}

export default FeedScreen;
