import React, { Component } from 'react';
import { connect } from 'react-redux';

import { AppState } from '../../config/redux/rootReducer';
import { NearEarthObject } from '../../constants/types';
import { getNEOFeedRequest } from '../../ducks/feed/actions';
import { getIsGettingNEOFeed, getNEOFeed } from '../../ducks/feed/selectors';

import FeedScreen from './feed';

interface Props {
  NEOfeed: NearEarthObject[],
  isGettingFeed: boolean;
  getFeed: (data: {
    start_date?: string,
    end_date?: string,
  }) => void;
}

interface State {
  startDate: Date,
  endDate: Date,
  searchValue: string,
}

class FeedContainer extends Component<Props, State> {

  state = {
    startDate: undefined,
    endDate: undefined,
    searchValue: '',
  }

  componentDidMount() {
    this.loadFeed();
  }

  loadFeed = () => {
    const { getFeed } = this.props;
    const { startDate, endDate } = this.state;

    getFeed({
      start_date: startDate,
      end_date: endDate,
    });
  };

  onSelectStartDate = (date: Date) => {
    this.setState(
      { startDate: date },
      () => this.loadFeed(),
    );
  }

  onSelectEndDate = (date: Date) => {
    this.setState(
      { endDate: date },
      () => this.loadFeed(),
    );
  }

  onRefresh = () => {
    this.setState({
      startDate: undefined,
      endDate: undefined,
    },
    () => this.loadFeed());
  }

  onChangeText = (text: string) => {
    this.setState({
      searchValue: text.toLowerCase(),
    });
  }

  onClearText = () => {
    this.setState({
      searchValue: '',
    });
  }

  render() {
    const { NEOfeed, isGettingFeed } = this.props;
    const { startDate, endDate, searchValue } = this.state;

    return (
      <FeedScreen
        data={NEOfeed?.filter(item => item.name.toLowerCase().includes(searchValue))}
        isGettingFeed={isGettingFeed}
        isRefreshing={isGettingFeed}
        onRefresh={this.onRefresh}
        startDate={startDate}
        endDate={endDate}
        searchValue={searchValue}
        onSelectStartDate={this.onSelectStartDate}
        onSelectEndDate={this.onSelectEndDate}
        onChangeText={this.onChangeText}
        onClearText={this.onClearText}
      />
    );
  }
}

const mapStateToProps = (state: AppState) => ({
  NEOfeed: getNEOFeed(state),
  isGettingFeed: getIsGettingNEOFeed(state),
});

const mapDispatchToProps = {
  getFeed: getNEOFeedRequest,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FeedContainer);
