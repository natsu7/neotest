import React from 'react';
import { View } from 'react-native';

import styles from './styles';
import Row from '../Row';
import { convertBoolToText } from '../../../../utils/helpers';

type Props = {
  id: string,
  name: string,
  estimated_diameter: { min: number, max: number }
  is_potentially_hazardous: boolean,
  is_sentry_object: boolean,
  relative_velocity: number,
  miss_distance: number,
  visual_magnitude?: number,
  close_approach_date_full: string,
}

const NEOListItem: React.FC<Props> = ({
  id,
  name,
  estimated_diameter,
  is_potentially_hazardous,
  is_sentry_object,
  miss_distance,
  relative_velocity,
  close_approach_date_full,
}) => (
  <View style={styles.container}>
    <Row title="id" value={id} />
    <Row title="name" value={name} />
    <Row title="est. diameter, m" value={`${estimated_diameter.min} - ${estimated_diameter.max}`} />
    <Row title="potentially hazardous" value={convertBoolToText(is_potentially_hazardous)} />

    <Row title="is sentry object" value={convertBoolToText(is_sentry_object)} />
    <Row title="miss distance" value={miss_distance} />
    <Row title="velocity" value={relative_velocity} />
    <Row title="close approach date" value={close_approach_date_full} />
  </View>
);

export default NEOListItem;
