import { StyleSheet } from 'react-native';

import { scale } from '../../../../utils/responsiveDimensions';
import { Colors } from '../../../../constants';

export default StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    borderRadius: 4,
    paddingVertical: scale(16),
    paddingLeft: scale(10),
    borderWidth: 1,
    borderColor: Colors.gray,
  },
});
