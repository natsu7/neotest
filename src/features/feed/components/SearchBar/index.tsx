import React from 'react';
import { View, Image, TextInput, StyleProp, ViewStyle } from 'react-native';

import { Images, Colors } from '../../../../constants';
import ButtonWithImage from '../ButtonWithImage';

import styles from './styles';

interface Props {
  containerStyle?: StyleProp<ViewStyle>;
  value: string;
  isDisabled?: boolean;
  onChangeText: (text: string) => void;
  onClearPress: () => void;
  onSubmit?: () => void;
}

const SearchBar: React.FC<Props> = ({
  containerStyle,
  value,
  isDisabled,
  onChangeText,
  onClearPress,
  onSubmit,
}) => (
  <View style={[styles.container, containerStyle]}>
    <Image source={Images.search} style={styles.searchIcon} />
    <TextInput
      style={styles.input}
      editable={!isDisabled}
      autoCorrect={false}
      autoCapitalize="none"
      underlineColorAndroid={Colors.transparent}
      value={value}
      placeholder="Search"
      placeholderTextColor={Colors.search}
      onChangeText={onChangeText}
      onSubmitEditing={onSubmit}
    />
    {!!value && (
      <ButtonWithImage
        imageSource={Images.closeBorderless}
        containerStyle={styles.clearButton}
        imageStyle={styles.clearIcon}
        disabled={isDisabled}
        onPress={onClearPress}
      />
    )}
  </View>
);

export default SearchBar;
