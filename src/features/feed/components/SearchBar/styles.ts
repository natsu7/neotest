import { StyleSheet } from 'react-native';

import { scale } from '../../../../utils/responsiveDimensions';
import { Colors } from '../../../../constants';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: scale(36),
    width: '85%',
    borderRadius: 10,
    marginBottom: scale(10),
    alignItems: 'center',
    backgroundColor: Colors.searchBackground,
  },
  input: {
    borderRadius: 10,
    flexDirection: 'row',
    height: 44,
    flex: 1,
    color: Colors.black,
    fontSize: scale(15),
  },
  inputActive: {
    marginLeft: 30,
    textAlign: 'left',
    marginRight: 30,
  },
  searchIcon: {
    marginLeft: scale(15),
    marginRight: scale(15),
    width: scale(14),
    height: scale(14),
  },
  iconActive: {
    left: 10,
  },
  clearButton: {
    width: scale(20),
    height: scale(20),
    marginRight: scale(15),
    backgroundColor: Colors.gray,
    borderRadius: 29,
    alignItems: 'center',
    justifyContent: 'center',
  },
  clearIcon: {
    width: scale(10),
    height: scale(10),
    tintColor: Colors.backgroundGray,
  },
});