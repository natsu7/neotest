import React from 'react';
import {
  TouchableOpacity, Image, StyleProp, ViewStyle, ImageStyle, TextStyle, ImageResizeMode, StyleSheet, Text,
} from 'react-native';

import styles from './styles';
import LoadingWrapper from '../LoadingWrapper';

interface Props {
  imageSource: number | object;
  imageStyle?: StyleProp<ImageStyle>;
  containerStyle?: StyleProp<ViewStyle>;
  textStyle?: StyleProp <TextStyle>;
  text?: string;
  activeOpacity?: number;
  backgroundColor?: string;
  resizeMode?: ImageResizeMode;
  loaderSize?: 'small' | 'large',
  loaderColor?: string,
  isLoading?: boolean;
  disabled?: boolean;
  onPress?: () => void;
}

const ButtonWithImage : React.FC<Props> = ({
  containerStyle,
  textStyle,
  imageStyle,
  activeOpacity = 0.5,
  text,
  imageSource,
  backgroundColor,
  resizeMode = 'contain',
  loaderSize,
  loaderColor,
  disabled,
  isLoading,
  onPress,
}) => {
  return (
    <TouchableOpacity
      style={StyleSheet.flatten([{ backgroundColor }, containerStyle])}
      disabled={disabled}
      activeOpacity={activeOpacity}
      onPress={onPress}
    >
      <LoadingWrapper isLoading={isLoading} size={loaderSize} color={loaderColor}>
        <Image
          style={StyleSheet.flatten([styles.image, imageStyle])}
          resizeMode={resizeMode}
          source={imageSource}
        />
      </LoadingWrapper>
      {!!text && (
        <Text style={StyleSheet.flatten([styles.text, textStyle])}>{text}</Text>
      )}
    </TouchableOpacity>
  );
};

export default ButtonWithImage;
