import { StyleSheet } from 'react-native';

import { scale } from '../../../../utils/responsiveDimensions';

export default StyleSheet.create({
  image: {
    width: scale(30),
    height: scale(30),
  },
  text: {},
});
