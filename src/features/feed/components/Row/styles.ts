
import { StyleSheet } from 'react-native';

import { Colors } from '../../../../constants';
import { scale } from '../../../../utils/responsiveDimensions';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: scale(3),
  },
  title: {
    fontWeight: 'bold',
    fontSize: scale(15),
    color: Colors.black,
    marginRight: scale(10),
  },
  value: {
    fontSize: scale(13),
    color: Colors.black,
    textAlign: 'right',
  },
});