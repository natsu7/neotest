import React from 'react';
import { View, Text, StyleProp, TextStyle, ViewStyle } from 'react-native';

import styles from './styles';

interface Props {
  title: string;
  value?: string | number;
  numberOfLines?: number;
  containerStyle?: StyleProp<ViewStyle>;
  titleStyle?: StyleProp<TextStyle>;
  valueStyle?: StyleProp<TextStyle>;
  valueComponent?: Element;
}

const Row: React.FC<Props> = ({ title='', value='', numberOfLines, containerStyle, titleStyle, valueStyle, valueComponent }) => (
  <View style={[styles.container, containerStyle]}>
    <Text style={[styles.title, titleStyle]}>{title}:</Text>
    {valueComponent || (
    <Text
      style={[styles.value, valueStyle]}
      numberOfLines={numberOfLines}
    >{value}
    </Text>
    )}
  </View>
);

export default Row;
