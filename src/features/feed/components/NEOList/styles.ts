import { StyleSheet } from 'react-native';

import { scale } from '../../../../utils/responsiveDimensions';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    justifyContent: 'space-between',
    paddingTop: scale(10),
    paddingBottom: scale(60),
    paddingHorizontal: scale(5),
  },
  columnSeparator: {
    height: scale(6),
  },
});
