import React from 'react';
import { View, FlatList, RefreshControl } from 'react-native';

import { NearEarthObject } from '../../../../constants/types';
import styles from './styles';
import NEOListItem from '../NEOListItem';


interface Props {
  data: NearEarthObject[];
  isRefreshing: boolean;
  onRefresh: () => void;
}

const renderItem = ({ item }: {item: NearEarthObject}) => (
  <NEOListItem
    id={item.id}
    name={item.name}
    estimated_diameter={item.estimated_diameter}
    is_potentially_hazardous={item.is_potentially_hazardous}
    is_sentry_object={item.is_sentry_object}
    miss_distance={item.miss_distance}
    close_approach_date_full={item.close_approach_date_full}
    relative_velocity={item.relative_velocity}
  />
);

const renderSeparator = () => <View style={styles.columnSeparator} />;


const NEOList: React.FC<Props> = ({ data, isRefreshing, onRefresh }) => (
  <FlatList
    style={styles.container}
    contentContainerStyle={styles.content}
    showsVerticalScrollIndicator={false}
    data={data}
    keyExtractor={item => `${item.id}`}
    renderItem={({ item }) => renderItem({ item })}
    ItemSeparatorComponent={renderSeparator}
    refreshControl={(
      <RefreshControl
        refreshing={isRefreshing}
        onRefresh={onRefresh}
      />
    )}
  />
);

export default NEOList;
