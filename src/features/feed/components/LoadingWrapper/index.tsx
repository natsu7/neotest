import React, { Fragment } from 'react';
import { View, ActivityIndicator, StyleProp, ViewStyle } from 'react-native';

import { Colors } from '../../../../constants';
import styles from './styles';

interface Props {
  containerStyle?: StyleProp<ViewStyle>,
  size?: 'small' | 'large',
  color?: string,
  isLoading: boolean,
  children: object,
}

const LoadingWrapper: React.FC<Props> = ({
  containerStyle,
  size = 'large',
  color = Colors.accentBlue,
  isLoading,
  children,
}) => (
  <Fragment>
    {!isLoading ? (
      children
    ) : (
      <View style={[styles.container, containerStyle]}>
        <ActivityIndicator color={color} size={size} />
      </View>
    )}
  </Fragment>
);


export default LoadingWrapper;
