import React, { useState } from 'react';
import { View, StyleProp, ViewStyle } from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';

import { formatDateForDisplay } from '../../../../utils/date';

import ButtonWithText from '../ButtonWithText';
import styles from './styles';

interface Props {
  date: Date;
  minDate?: Date;
  maxDate?: Date;
  placeholderText?: string;
  containerStyle?: StyleProp<ViewStyle>;
  onDateSelected: (date: Date) => void;
}

const DatePicker: React.FC<Props> = ({ date, minDate, maxDate, placeholderText, containerStyle, onDateSelected }) => {
  const [isDatePickerVisible, toggleDatePicker] = useState(false);

  const showDatePicker = () => {
    toggleDatePicker(true);
  };

  const hideDatePicker = () => {
    toggleDatePicker(false);
  };

  const handleSelectedDate = (selectedDate: Date) => {
    hideDatePicker();
    onDateSelected(selectedDate);
  };

  return (
    <View>
      <ButtonWithText
        containerStyle={[styles.dateButton, containerStyle]}
        textStyle={styles.dateText}
        text={
          date
            ? formatDateForDisplay(date)
            : placeholderText || 'select date'
        }
        onPress={showDatePicker}
      />
      <DateTimePickerModal
        date={date ? new Date(date) : new Date()}
        maximumDate={maxDate}
        minimumDate={minDate}
        isVisible={isDatePickerVisible}
        mode="date"
        onConfirm={handleSelectedDate}
        onCancel={hideDatePicker}
      />
    </View>
  );
};

export default DatePicker;
