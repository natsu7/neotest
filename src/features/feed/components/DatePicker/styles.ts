import { StyleSheet } from 'react-native';
import { scale } from '../../../../utils/responsiveDimensions';
import { Colors } from '../../../../constants';

export default StyleSheet.create({
  dateButton: {
    height: scale(50),
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: Colors.rockBlue,
    borderRadius: 4,
    paddingHorizontal: scale(5),
    width: scale(165),
    backgroundColor: Colors.white,
  },
  dateText: {
    fontSize: scale(15),
    color: Colors.denimBlue,
  },
});

