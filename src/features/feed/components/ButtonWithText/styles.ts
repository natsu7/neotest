import { StyleSheet } from 'react-native';

import { Colors } from '../../../../constants';
import { scale } from '../../../../utils/responsiveDimensions';


export default StyleSheet.create({
  container: {
    width: '100%',
    height: scale(50),
    borderRadius: scale(4),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.accentBlue,
  },
  disabled: {
    opacity: 0.7,
  },
  text: {
    fontSize: scale(15),
    color: Colors.white,
  },
});