import { StyleSheet } from 'react-native';

import { scale } from '../../utils/responsiveDimensions';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: scale(10),
    paddingHorizontal: scale(16),
  },
  datePickersContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
