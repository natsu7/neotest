import apisauce from 'apisauce';

import { API_KEY } from '../config/keys';
import { formatDateForAPI } from '../utils/date';

const api = apisauce.create({
  baseURL: 'https://api.nasa.gov/neo/rest/v1',
  headers: {
    Accept: 'application/json',
    'Content-type': 'application/json',
  },
  timeout: 20000,
});

export default class ServiceAPI {

  static getFeed = ({
    start_date,
    end_date,
  }: {
    start_date?: string,
    end_date?: string
  }) => {
    let requestString = `/feed?api_key=${API_KEY}`;

    if(start_date) {
      requestString = requestString.concat(`&start_date=${formatDateForAPI(start_date)}`);
    }

    if(end_date) {
      requestString = requestString.concat(`&end_date=${formatDateForAPI(end_date)}`);
    }

    return api.get(requestString);
  }
}
